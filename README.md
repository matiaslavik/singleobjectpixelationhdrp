# ProgrammingBlog

This is an example project showing how to use custom passes in Unity's High Definition Render Pipeline.

See full blog post here: https://matiaslavik.codeberg.page/unity-hdrp-custompass/

<img src="https://matiaslavik.codeberg.page/images/hdrp-custompass/result.jpg" width="500px">

